# Go linter

This job configuration allows you to linter-test your Golang application.

### How to include
```yml
stages:
  - test

include:
  - project: 'aanaz/devops-includes/gitlab-ci-extends/includes/go-linter'
    ref: main
    file: go-linter.yml

go-linter:
  extends: .go-linter
```
